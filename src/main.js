import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import './plugins/fontawesome'
import './plugins/vuetify'
import App from './App.vue'
import Vuetify from 'vuetify'
import router from './routers/router'


Vue.config.productionTip = false;

Vue.use(Vuetify);

new Vue({
  router,
  render: h => h(App),
  history:true
}).$mount('#app');
