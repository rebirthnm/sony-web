import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
    base: '/microsite/sonydays2019/',
    mode: 'history',
    routes: [
        /*
        |--------------------------------------------------------------------------
        | Frontend Routes
        |--------------------------------------------------------------------------|
        */
        {
            path: '/',
            component: () => import('../App'),
            children: [
                {
                    path: '/',
                    name: 'Home',
                    component: () => import('../views/TV')
                },
                {
                    path: '/products-tv',
                    name: 'TV',
                    component: () => import('../views/TV')
                },
                {
                    path: '/products-headphone',
                    name: 'Headphone',
                    component: () => import('../views/Headphone')
                },
                {
                    path: '/products-camera',
                    name: 'Camera',
                    component: () => import('../views/Camera')
                },
                {
                    path: '/products-sound',
                    name: 'Sound',
                    component: () => import('../views/Sound')
                },
                {
                    path: '/products-lens',
                    name: 'Lens',
                    component: () => import('../views/Lens')
                },
                {
                    path: '/products-playstation',
                    name: 'PlayStation',
                    component: () => import('../views/Playstation')
                },
            ]
        },
    ]
})
